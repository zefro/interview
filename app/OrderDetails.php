<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $fillable = ['id','order_id','product_id ','status','token'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}
