<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Product::create([
            'product_id' => '234567',
            'name' => '1',
            'description'=>'test',
            'quantity'=>'123',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => '1',
            'description'=>'test',
            'quantity'=>'123',
            'status'=>'2'
        ]);
        
        App\Product::create([
            'product_id' => 'rfghtyui',
            'name' => 'lamp',
            'description'=>'test',
            'quantity'=>'2005',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => 'rtyui',
            'name' => 'lamp',
            'description'=>'test',
            'quantity'=>'7600',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => 'fgh',
            'name' => 'lamp',
            'description'=>'test',
            'quantity'=>'7600',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'phone',
            'description'=>'asdfghjk',
            'quantity'=>'12',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'phone',
            'description'=>'sdfgh',
            'quantity'=>'99',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'laptop',
            'description'=>'test',
            'quantity'=>'51',
            'status'=>'2'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
        App\Product::create([
            'product_id' => '234567',
            'name' => 'juice',
            'description'=>'test',
            'quantity'=>'30000',
            'status'=>'1'
        ]);
    }
}
