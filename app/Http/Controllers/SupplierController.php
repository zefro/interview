<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{

    //get all suppliers
    public function getSuppliers(){
        $suppliers = Supplier::all(); 
        return view('supplier.all_suppliers')->with('suppliers',$suppliers);;
    }
    
    //get specific supplier
    public function getSupplierById($id){
        $supplier = Supplier::find($id);
        if(is_null($supplier)){
            return response()->json(['message'=>'supplier not found'], 404);
        }
            return response()->json($supplier::find($id), 200);
        
    }

    //form to add new suppliers
    public function newSupplierForm(){
        return view('supplier.new_supplier_form');
    }

    
    //add supplier
    public function createSupplier(Request $request){
        $supplier = new Supplier();
            $supplier->name = $request->name;
            $supplier->status = 1;
            $supplier->save();

            return redirect('/api/suppliers')->with('success', ' Added successfully');
    }
    //deactivate supplier
    public function deactivateSupplier($id){
            $supplier=Supplier::find($id);
            $supplier->status= 2;
            $supplier->save();
            return redirect('/api/suppliers')->with('success', ' Deactivated successfully');
    }
    public function activateSupplier($id){
        $supplier=Supplier::find($id);
        $supplier->status= 1;
        $supplier->save();
        return redirect('/api/suppliers')->with('success', ' activated successfully');
}

    public function edit($id){
        
        $supplier = Supplier::find($id);
        return view('supplier.edit', compact('supplier','id'));
    }

    public function update(Request $request , $id){
        
        $supplier = Supplier::find($id);
        $supplier->name = $request->get('name');
        $supplier->save();
        return redirect('/api/suppliers')->with('success','Data Updated');
    }

}
