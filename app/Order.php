<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['id','order_number','status','token'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}
