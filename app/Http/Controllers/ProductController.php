<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{

    //get all orders
    public function getProducts(){
        $products = Product::all(); 
        return view('product.all_products')->with('products',$products);;
    }
    // public function chartDetails(){
    //     $products = Product::all(); 
    //     return view('charts.index')->with('products',$products);;
    // }
    
    //get specific orders
    public function getProductById($id){
        $products = Products::find($id);
        if(is_null($products)){
            return response()->json(['message'=>'products not found'], 404);
        }
            return response()->json($products::find($id), 200);
        
    }

    //form to add new orders
    public function newProductForm(){
        return view('product.new_product_form');
    }

    
    //add orders
    
    public function createProduct(Request $request){
        $product_id = $this->generatePin();

        $products = new Product();
            $products->name = $request->name;
            $products->product_id = $product_id;
            $products->description = $request->description;
            $products->quantity = $request->quantity;
            $products->status = 1;
            $products->save();

            return redirect('/api/products')->with('success', ' Added successfully');
    }
    //deactivate orders
    public function deactivateProduct($id){
            $products=Product::find($id);
            $products->status= "delivered";
            $products->save();
            return redirect('/api/products')->with('success', ' Deactivated successfully');
    }
    public function activateProductproducts($id){
        $products=Products::find($id);
        $products->status= "pending";
        $products->save();
        return redirect('/api/products')->with('success', ' activated successfully');
}

    public function edit($id){
        
        $products = Product::find($id);
        return view('product.edit', compact('products','id'));
    }

    public function update(Request $request , $id){
        
        $products = Product::find($id);
        $products->name = $request->get('name');
        $products->save();
        return redirect('/api/products')->with('success','Data Updated');
    }

    //code to generate a unique product id
    function generatePin($length = 7)
    {
        $chars = '0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

}
