<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/form.css') }}">

	<div class="container">
			<div class="main">
				<div class="main-center">
					<form class="" method="post" action="{{url('/api/order/update', $id)}}">
                    {{ csrf_field() }}
						<div class="form-group">
							<label for="name">PRODUCT ID</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <div  class="form-group">
                                    <div class="cols-sm-10">
									<select class="form-control" id="product_id" name="product_id" required>
										<option value="0" disabled="true" selected="true">--SELECT PRODUCT--</option>
              						@foreach ($products as $product)
              							 <option value="{{$product->product_id}}">{{$product->name }}</option>
              						@endforeach
      								</select>
						  		</div>

                        </div>
                        </div>
                        </br>
                        <div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <div  class="form-group">
                            <input type="text" name="name" class="form-control" value="{{$products->quantity}}"
                            placeholder="Enter New Quantity" />
                        </div>
                        </div>
                        </br>
                        <div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <div  class="form-group">
                            <input type="text" name="name" class="form-control" value="{{$products->description}}"
                            placeholder="Enter New Description" />
                        </div>
						</div>
						</div>
                        </br>

						<button type="submit" class="btn btn-primary">Submit</button>
						
					</form>
				</div><!--main-center"-->
			</div><!--main-->
		</div><!--container-->