<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('suppliers','SupplierController@getSuppliers');
Route::get('supplier/{id}','SupplierController@getSupplierById');
Route::get('suppliers/form','SupplierController@newSupplierForm')->name('supplier.form');
Route::post('supplier/new','SupplierController@createSupplier')->name('new.supplier.form');
Route::get('supplier/deactivate/{id}','SupplierController@deactivateSupplier')->name('deactivate.supplier');
Route::get('supplier/activate/{id}','SupplierController@activateSupplier')->name('deactivate.supplier');
Route::get('supplier/edit/{id}', 'SupplierController@edit')->name('edit.supplier');
Route::post('supplier/update/{id}', 'SupplierController@update')->name('update.supplier');


//products
Route::get('products','ProductController@getProducts');
Route::get('product/{id}','ProductController@getProductById');
Route::get('products/form','ProductController@newProductForm')->name('product.form');
Route::post('product/new','ProductController@createProduct')->name('new.product.form');
Route::get('product/deactivate/{id}','ProductController@deactivateProduct')->name('deactivate.product');
Route::get('product/activate/{id}','ProductController@activateProduct')->name('deactivate.product');
Route::get('product/edit/{id}', 'ProductController@edit')->name('edit.product');
Route::post('product/update/{id}', 'ProductController@update')->name('update.product');


//orders
Route::get('orders','OrderController@getOrders');
Route::get('order/{id}','OrderController@getOrderById');
Route::get('orders/form','OrderController@newOrderForm')->name('order.form');
Route::post('order/new','OrderController@createOrder')->name('new.order.form');
Route::get('order/deactivate/{id}','OrderController@deactivateOrder')->name('deactivate.order');
Route::get('order/activate/{id}','OrderController@activateOrder')->name('deactivate.order');
Route::post('order/update/{id}', 'OrdertController@update')->name('update.order');

Route::get('/reports', 'ChartController@generateChart')->name('generate.chart');