<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
<!--table-->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

        
        
        <nav class="navbar navbar-expand navbar-dark bg-primary"> <a href="#menu-toggle" id="menu-toggle" class="navbar-brand"><span class="navbar-toggler-icon"></span></a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse" id="navbarsExample02">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"> <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> </li>

                </ul>
                <form class="form-inline my-2 my-md-0"> </form>
            </div>
        </nav>
        <div id="wrapper" class="toggled">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand"> <a href="/home"> Home </a> </li>
                    <li> <a href="/api/suppliers">Suppliers</a> </li>
                    <li> <a href="/api/orders">Orders</a> </li>
                    <li> <a href="/api/products">Products</a> </li>
                    <li> <a href="/api/reports">Reports</a> </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="container">
                    <div class="row col-md-6 col-md-offset-2 custyle">
                    <table class="table table-striped custab">
                    <thead>
                    <a href="/api/suppliers/form" class="btn btn-info btn-xs pull-right"><b>+</b> Add New Company</a>
                        <tr>
                            <th>ID</th>
                            <th>COMPANY NAME</th>
                            <th>STATUS</th>
                            <th>EDIT</th>
                        </tr>
                    </thead>
                    @foreach($suppliers as $supplier)
                        <tr>
                            <td>{{$supplier->id}}</td>
                            <td>{{$supplier->name}}</td>
                            <td>
                                @if ($supplier->status == 1)
                                <a href="{{ url('api/supplier/deactivate',$supplier->id)}}" class="btn btn-danger">Deactivate</a>
                                @elseif ($supplier->status == 2)
                                <a href="{{ url('api/supplier/activate',$supplier->id)}}" class="btn btn-success">Activate</a>
                                @endif
                            </td>
                            <td>
                            <a href="{{ url('api/supplier/edit',$supplier->id)}}" class="btn btn-info">Update</a>
                            </td>
                </tr>
                    @endforeach                              
                    </table>
                    </div>
                </div>
                </div>
        </div>
            </div> <!-- /#page-content-wrapper -->
        </div> <!-- /#wrapper -->
        <!-- Bootstrap core JavaScript -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script> <!-- Menu Toggle Script -->
        <script>
          $(function(){
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

            $(window).resize(function(e) {
              if($(window).width()<=768){
                $("#wrapper").removeClass("toggled");
              }else{
                $("#wrapper").addClass("toggled");
              }
            });
          });
           
        </script>
   
  </html>