<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierProduct extends Model
{
    protected $fillable = ['id','supply_id','product_id','status','token'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}
