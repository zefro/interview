<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/form.css') }}">

	<div class="container">
			<div class="main">
				<div class="main-center">
					<form class="" method="post" action="{{ route('new.product.form') }}">
                    {{ csrf_field() }}
						<div class="form-group">
							<label for="name">Product Details</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter Product Name"/>
							</div>
							</br>
							<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="quantity" id="quantity"  placeholder="Enter Product Quantity"/>
							</div>
							</br>
							<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="description" id="description"  placeholder="Enter Product Description"/>
							</div>
							
						</div>


						<button type="submit" class="btn btn-primary">Submit</button>
						
					</form>
				</div><!--main-center"-->
			</div><!--main-->
		</div><!--container-->