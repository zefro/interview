<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected  $fillable = ['name','token','status'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}
