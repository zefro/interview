<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('product_id');
            $table->string('name');
            $table->Longtext('description');
            $table->string('quantity');
            $table->string('status');
            $table->string('updated_at')->nullable();;
            $table->timestamp('deteled_at')->nullable();;
            $table->timestamp('created_at')->nullable();
            $table->string('token')->nullable();;
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
