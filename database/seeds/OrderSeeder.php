<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Order::create([
            'order_number' => '3456789',
            'status' => '1',
        ]);
        App\Order::create([
            'order_number' => '45678',
            'status' => '1',
        ]);
        App\Order::create([
            'order_number' => '345678',
            'status' => '1',
        ]);
        App\Order::create([
            'order_number' => '345678',
            'status' => '1',
        ]);
        App\Order::create([
            'order_number' => '234567',
            'status' => '1',
        ]);
    }
}
