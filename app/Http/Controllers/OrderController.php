<?php

namespace App\Http\Controllers;
use App\OrderDetails;
use App\Product;

use Illuminate\Http\Request;

class OrderController extends Controller
{

    //get all orders
    public function getOrders(){
        $orders = OrderDetails::all(); 
        return view('order.all_orders')->with('orders',$orders);;
    }
    
    //get specific orders
    public function getOrderById($id){
        $orders = OrderDetails::find($id);
        if(is_null($orders)){
            return response()->json(['message'=>'Order not found'], 404);
        }
            return response()->json($orders::find($id), 200);
        
    }

    //form to add new orders
    public function newOrderForm(){   
            $products = Product::all(); 
        return view('order.new_order_form')->with('products',$products);
    }

    
    //add orders
    public function createOrder(Request $request){

        //code to generate a unique order id
     function generatePin($length = 7)
     {
         $chars = '0123456789';
         $count = mb_strlen($chars);
 
         for ($i = 0, $result = ''; $i < $length; $i++) {
             $index = rand(0, $count - 1);
             $result .= mb_substr($chars, $index, 1);
         }
         return $result;
     }
        $orders = new OrderDetails();

        $order_id = generatePin();
            $orders->product_id = $request->product_id;
            $orders->order_id = $order_id;
            $orders->status = 1;
            $orders->save();

            return redirect('/api/orders')->with('success', ' Added successfully');
    }
    //deactivate orders
    public function deactivateOrder($id){
            $orders=OrderDetails::find($id);
            $orders->status= "delivered";
            $orders->save();
            return redirect('/api/orders')->with('success', ' Deactivated successfully');
    }
    public function activateOrder($id){
        $orders=OrderDetails::find($id);
        $orders->status= "pending";
        $orders->save();
        return redirect('/api/orders')->with('success', ' activated successfully');
}

    

    public function update(Request $request , $id){
        
        $orders = OrderDetails::find($id);
        $orders->name = $request->get('name');
        $orders->save();
        return redirect('/api/orders')->with('success','Data Updated');
    }
     
 }
 
