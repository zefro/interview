<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id','product_id','name','description','quantity','status','token'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}
